__author__ = 'justingibbons'

def dic_value_starts_with(dic,key,starts_with_string):
    """Take a dictionary and returns true if the value stored under key starts with the
        starts_with_string"""

    if dic[key].startswith(starts_with_string):
        return True
    else:
        return False


if __name__=="__main__":
    import unittest
    dic={"first":"ABC123","second":"abc123"}
    class TestDicValueStartsWith(unittest.TestCase):

        def test_true(self):
            self.assertEqual(dic_value_starts_with(dic,key="first",starts_with_string="ABC"),True)
        def test_false(self):
            self.assertEqual(dic_value_starts_with(dic,key="second",starts_with_string="ABC"),False)


    unittest.main()