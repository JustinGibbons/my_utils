__author__ = 'justingibbons'
import csv

def group_by(input_file,output_file,skip_header=True):
    """Takes a csv file and creates a new csv file were the rows are the values from the first column grouped
        by the values in the second column"""
    groups_dic=dict()
    if skip_header==True:
        header_found=False
    else:
        header_found=True

    with open(input_file,"r") as infile:
        csv_reader=csv.reader(infile)
        for row in csv_reader:
            if header_found==False:
                header_found=True
                continue
            else:
                value=row[0]
                group=row[1]
                if group in groups_dic:
                    groups_dic[group].append(value)
                else:
                    groups_dic[group]=[value]
    with open(output_file,"w") as outfile:
        csv_writer=csv.writer(outfile)
        output=sorted(groups_dic.items(),key=lambda i: i[0])
        for item in output:
            csv_writer.writerow(item[1])


if __name__=="__main__":

    import unittest
    from compare_files_tools import get_file_lines

    class Test_Group_By(unittest.TestCase):
        def test_group_by(self):
            input="test_data/test1_input_groupby.csv"
            test_output="test_data/out_data/test1_test_output_groupby.csv"
            answer_file="test_data/out_data/test1_output_groupby.csv"
            group_by(input,test_output)
            test_output_lines,correct_output_lines=get_file_lines(test_output,answer_file)
            self.assertEqual(test_output_lines,correct_output_lines)

    unittest.main()
