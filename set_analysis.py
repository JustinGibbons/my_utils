__author__ = 'justingibbons'

def calculuate_tanimotot(set1,set2,round_to=2):
    """Calculates the tanimoto coefficient of 2 sets. round_to is the number of decimal places to round to"""
    return round(float(len(set1 & set2))/len(set1 | set2),ndigits=round_to)


if __name__=="__main__":
    import unittest

    class TestSetAnalysis(unittest.TestCase):
        def test_calculate_tanimoto(self):
            set1={1,2,3,4,5,6,7,8,9}
            set2={2,4,6,8}
            self.assertEqual(calculuate_tanimotot(set1,set2),0.44)
    unittest.main()