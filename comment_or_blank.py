def comment_or_blank(line,comment_character="#"):
    """Returns true is line startswith # (besides white space or is blank,
    otherwise returns the original line"""
    striped_line=line.strip()
    if striped_line.startswith(comment_character):
        return True
    elif striped_line=="":
        return True
    else:
        return line

if __name__=="__main__":
    import unittest

    class TestCommentorBlank(unittest.TestCase):
        def test_comment_first(self):
            self.assertTrue(comment_or_blank("#someotherstuff"))

        def test_blank_than_comment(self):
            self.assertTrue(comment_or_blank("   # otherstuff"))

        def test_just_blank(self):
            self.assertTrue(comment_or_blank("    "))

        def test_text(self):
            input_text="some text"
            self.assertEqual(comment_or_blank(input_text),input_text)

    unittest.main()