__author__ = 'justingibbons'

def report_group_tanimotos(group_data,round_to=3):
    """Takes in a dictionary were the key is a group id and the value
        is a dictionary containing the union of the group values and the
            interesction of the group values. Returns a dictionary with
            the tanimoto coefficient for the group"""
    intersection_key="intersection"
    union_key="union"
    zero_division_error_answer="NaN"
    tanimoto_dic=dict()
    for group in group_data:
        intersection=group_data[group][intersection_key]
        union=group_data[group][union_key]
        try:
            tanimoto_dic[group]=round(float(len(intersection))/len(union),ndigits=round_to)
        except ZeroDivisionError:
            tanimoto_dic[group]=zero_division_error_answer


    return tanimoto_dic

if __name__=="__main__":
    import unittest

    class TestGroupAnalysisTools(unittest.TestCase):
        def test_report_group_tanimotos(self):
            input={1:{"union":{1,2,3,4,5,6},
                      "intersection":{2,4,6}},
                   2:{"union":{1,3,5},
                      "intersection":{1}}}
            correct_answer={1:0.500,
                            2:0.333}

            self.assertEqual(report_group_tanimotos(input),correct_answer)

    unittest.main()