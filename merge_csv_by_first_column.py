__author__ = 'justingibbons'

import csv
import sys

def get_header_and_data_dictionary(csv_file,column_key=0,sep=",",dialect="excel",read_mode="rb"):
    """Takes a csv file and creates a dictionary where the value in the column_key index is used as the
    dictionary key and the value is the row as a list."""

    data_dict=dict()
    #Get the data
    header_found=False
    with open(csv_file,read_mode) as csv_obj:
        csv_reader=csv.reader(csv_obj,delimiter=sep,dialect=dialect)
        try:
            for row in csv_reader:
                if header_found==False:
                    header=row
                    header_found=True
                else:
                    #Get the key value:
                    key=row[column_key]
                    data_dict[key]=row
        except csv.Error, e:
            sys.exit("file %s, line %d: %s" %(csv_file,csv_reader.line_num,e))

    return (header,data_dict)

def merge_csv_by_column(csv1,csv2,outfile,column_key1=0,column_key2=0):
    """Takes 2 csv files which may have different headers and combines the rows where the values in the
    column with the column_key index are the same (defaults to first column). If the header value for
    the column_key column is different in the two files then defaults to the one used in csv1. This is only
    really tested on the first column being the key to keep things simple"""

    #Get the data
    header1,data1=get_header_and_data_dictionary(csv1,column_key1)
    header2,data2=get_header_and_data_dictionary(csv2,column_key2)
    #Combine the data

    #First combine the headers
    #The header value coorsponding to column_key2 must be removed
    del header2[column_key2]
    header1.extend(header2)
    with open(outfile,"w") as write_obj:
        csv_writer=csv.writer(write_obj)
        csv_writer.writerow(header1) #First create the header

    #Combine the rows with the matching column_key values but first remove the column_key2 value to avoid redundancy
        for key in data1:
            if key in data2:
                del data2[key][column_key2]
                data1[key].extend(data2[key])
                csv_writer.writerow(data1[key])


if __name__=="__main__":
    csv1_file="test_data/sample_csv1.csv"
    csv2_file="test_data/sample_csv2.csv"
    outfile="test_data/out_data/test_output_merge_csv_by_first_column.csv"

    merge_csv_by_column(csv1_file,csv2_file,outfile)


