__author__ = 'justingibbons'
import csv
import json
from json_utils import JSON_Extensions
from collections import Counter

def make_list_items_different(in_list):
    """Takes in a list and concatenates numbers to the end of it until all of the values in the list are
    unique. Not doing exactly what I would like it to do (see the unittest but it seems good enough"""

    number_of_times_found=dict() # A dic to keep track of the number of times the header was found
    histogram=Counter(in_list)

    for index in range(len(in_list)):
        key=in_list[index]
        if histogram[key]>1:
            if key in number_of_times_found:
                number_of_times_found[key]+=1
            else:
                number_of_times_found[key]=0
            new_key=key+"_"+str(number_of_times_found[key])
            #See if this new key was in the original data
            if new_key in histogram:
                histogram[new_key]+=1
                if new_key in number_of_times_found:
                    number_of_times_found[new_key]+=1
                else:
                    number_of_times_found[new_key]=0
            in_list[index]=new_key
    return in_list

class csv_to_json(JSON_Extensions):
    """Creates a json-like object out of a csv file. The rows in the
        csv become dictionarys in a list with the header used as the key
            for the data in that row's columns..
            If skip comments is True skips lines starting with #"""

    def __init__(self, csv_file, delimiter=",",make_sure_header_keys_unique=True,skip_comments=True):
        self.obj=[]
        header_found=False
        with open(csv_file,"rb") as in_csv:
            csv_reader=csv.reader(in_csv,delimiter=delimiter)
            for row in csv_reader:
                if row[0].startswith("#") and skip_comments:
                    continue
                if header_found==False:
                    self.header=row
                    header_found=True
                    if make_sure_header_keys_unique:
                        self.header=make_list_items_different(self.header)
                else:
                    self.obj.append(dict(zip(self.header,row)))






if __name__=="__main__":
    import unittest
    sample_tab_csv_file="test_data/sample_tab_sep_csv.csv"
    sample_dup_headers="test_data/sample_csv_dup_header.txt"
    sample_tab_with_comments="test_data/sample_tab_csv_with_comments.txt"
    obj=csv_to_json(sample_tab_csv_file,delimiter="\t")
    obj_dup_headers=csv_to_json(sample_dup_headers,delimiter="\t",make_sure_header_keys_unique=True)
    obj_comments=csv_to_json(sample_tab_with_comments,delimiter="\t")

    class Test_CSV_TO_JSON(unittest.TestCase):
        def test_make_list_items_different(self):
            input=["header1","header2","header3","header1","header4","header3","header1","header1_1"]
            output=["header1_0","header2","header3_0","header1_1","header4","header3_1","header1_2","header1_1_1"]
            self.assertEqual(make_list_items_different(input),output)

        def test_make_sure_header_keys_unique_option(self):
            correct=["header1_0","header2","header3_0","header1_1","header4","header3_1","header1_2","header1_1_1"]
            self.assertEqual(obj_dup_headers.header,correct)

        def test_object_creation_and_getitem(self):
            row1={"header1":"Row1.1","header2":"Row1.2","header3":"Row1.3","header4":"Row1.4"}
            row2={"header1":"","header2":"Row2.2","header3":"Row2.3","header4":"Row2.4"}
            row3={"header1":"Row3.1","header2":"Row3.2","header3":"Row3.3","header4":""}
            row4={"header1":"","header2":"Row4.2","header3":"","header4":""}
            self.assertEqual(obj[0],row1)
            self.assertEqual(obj[1],row2)
            self.assertEqual(obj[2],row3)
            self.assertEqual(obj[3],row4)
            self.assertNotEquals(obj[0],row2)

        def test_object_creation_and_getitem_with_comments(self):
            row1={"header1":"Row1.1","header2":"Row1.2","header3":"Row1.3","header4":"Row1.4"}
            row2={"header1":"","header2":"Row2.2","header3":"Row2.3","header4":"Row2.4"}
            row3={"header1":"Row3.1","header2":"Row3.2","header3":"Row3.3","header4":""}
            row4={"header1":"","header2":"Row4.2","header3":"","header4":""}
            self.assertEqual(obj_comments[0],row1)
            self.assertEqual(obj_comments[1],row2)
            self.assertEqual(obj_comments[2],row3)
            self.assertEqual(obj_comments[3],row4)
            self.assertNotEquals(obj_comments[0],row2)

        def test_eq(self):
            row1={"header1":"Row1.1","header2":"Row1.2","header3":"Row1.3","header4":"Row1.4"}
            row2={"header1":"","header2":"Row2.2","header3":"Row2.3","header4":"Row2.4"}
            row3={"header1":"Row3.1","header2":"Row3.2","header3":"Row3.3","header4":""}
            row4={"header1":"","header2":"Row4.2","header3":"","header4":""}
            correct_answer=[row1,row2,row3,row4]
            self.assertTrue(obj==correct_answer)


        def test_len(self):
            self.assertEqual(len(obj),4)

        def test_create_value_set(self):
            correct_answer={"Row1.1","","Row3.1"}
            self.assertEqual(obj.create_value_set("header1"),correct_answer)
        def test_write_to_json(self):
            outfile="test_data/out_data/sample_out_test_write_to_json"
            row1={"header1":"Row1.1","header2":"Row1.2","header3":"Row1.3","header4":"Row1.4"}
            row2={"header1":"","header2":"Row2.2","header3":"Row2.3","header4":"Row2.4"}
            row3={"header1":"Row3.1","header2":"Row3.2","header3":"Row3.3","header4":""}
            row4={"header1":"","header2":"Row4.2","header3":"","header4":""}
            obj.write_to_json(outfile)
            #Open the file as a json object to compare
            f=open(outfile,"r")
            out_obj=json.load(f)
            f.close()
            self.assertEqual(out_obj[0],row1)
            self.assertEqual(out_obj[1],row2)
            self.assertEqual(out_obj[2],row3)
            self.assertEqual(out_obj[3],row4)
            self.assertNotEquals(out_obj[0],row2)



    unittest.main()






