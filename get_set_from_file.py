__author__ = 'justingibbons'
import csv
def get_set_from_file(infile):
    """Reads in a file with a single column and creates a set from that file"""
    return_set=set()

    with open(infile,"r") as i:
        for line in i:
            return_set.add(line.strip())

    return return_set

def get_set_from_column(infile,header=True,delimiter=",",column_index=0):
    """Returns a set containing the values from the column at column_index"""
    return_set=set()
    if header==True:
        header_found=False
    else:
        header_found=True
    with open(infile,"r") as in_file:
        csv_reader=csv.reader(in_file,delimiter=delimiter)
        for line in csv_reader:
            if header_found:
                return_set.add(line[column_index])
            else:
                header_found=True
                continue

    return return_set


if __name__=="__main__":
    import unittest

    class TestGetSetFromFile(unittest.TestCase):
        def test_get_set_from_file(self):
            self.assertEqual(get_set_from_file("test_data/get_set_from_file_test_file.txt"),{"C","A","B","a","c","123","b"})

        def test_get_set_from_column(self):
            infile="test_data/test1_input_groupby.csv"
            correct_answer={"1","2","3"}
            self.assertEqual(get_set_from_column(infile,column_index=1),correct_answer)



    unittest.main()


