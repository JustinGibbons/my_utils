__author__ = 'justingibbons'
import json,csv
def write_json_to_file(object,outfile):
    """Takes a json like object and writes it to file"""
    f=open(outfile,"w")
    json.dump(object,f)
    f.close()

def open_json_object(infile):
    """Read in a json object from file"""
    f=open(infile,"r")
    object=json.load(f)
    f.close()
    return object

def write_json_to_csv(json_object,outfile,header,delimiter=","):
    """Creates a CSV of the json object using the header to order the data. Note only data included
    in the header will be written out."""
    with open(outfile,"w") as out_object:
        csv_writer=csv.writer(out_object,delimiter=delimiter)
        #First write the header
        csv_writer.writerow(header)
        for data in json_object:
            row=[]
            for key in header:
                row.append(data[key])

            csv_writer.writerow(row)
class JSON_Extensions:
    def __init__(self,json_or_json_like_object):
        self.obj=json_or_json_like_object


    def __str__(self):
        return str(self.obj)

    def __getitem__(self, item):
        return self.obj[item]

    def iter__(self):
        return iter(self.obj)

    def __len__(self):
        return len(self.obj)

    def __eq__(self,item):
        if str(self.obj)==str(item):
            return True
        else:
            return False

    def create_value_set(self,key):
        """Returns a set of all the values from a column with the header key"""
        return_set=set()
        for row in self.obj:
            return_set.add(row[key])
        return return_set


    def write_to_json(self,outfile):
        f=open(outfile,"w")
        json.dump(self.obj,f)
        f.close()

    def filter_by_value_in_other_json(self,other,key):
        """Creates a new json object containing the rows in self that have a value under key
        that is also found under that key in other"""
        rows_to_keep=[]
        other_value_set=other.create_value_set(key)
        for row in self.obj:
            if row[key] in other_value_set:
                rows_to_keep.append(row)
            else:
                continue
        return JSON_Extensions(rows_to_keep)

    def remove_values_in_set(self,key,removal_set):
        """Creates a new json like object by appending rows to a list only if the value stored under key
        is not in removal set"""
        rows_to_keep=[]

        for row in self.obj:
            if row[key] in removal_set:
                continue
            else:
                rows_to_keep.append(row)

        return JSON_Extensions(rows_to_keep)

    def keep_values_in_set(self,key,keep_set):
        """Creates a new json like object by appending rows to a list only if the value stored under key
        is in keep_set"""
        rows_to_keep=[]

        for row in self.obj:
            if row[key] in keep_set:
                rows_to_keep.append(row)
            else:
                continue

        return JSON_Extensions(rows_to_keep)


    def keep_n_values_in_set(self,key,keep_set,n):
        """Creates a new json like object by appending rows to a list only if that value is stored under key
        is in keep_set. Only returns up to the top n rows"""

        new_json=JSON_Extensions(self.obj).keep_values_in_set(key=key,keep_set=keep_set)
        if len(new_json)<n:
            return new_json
        else:
            return JSON_Extensions(new_json[0:n])


    def group_by(self,group_by_key,group_by_value):
        """Creates a dictionary where the keys are the values in the group by key and the values are a set containing
        the values stored under the group_by_value that co-occur with that the group_by_key value"""
        return_dic=dict()
        for row in self.obj:
            key=row[group_by_key]
            value=row[group_by_value]
            if key in return_dic:
                return_dic[key].add(value)
            else:
                return_dic[key]={value}
        return return_dic
    def create_value_union(self,other,key):
        """Returns a set containing the union of all the values pointed to by key"""
        pass

    def sort_by_key(self,sort_on_key,reverse=False):
        """Sorts the json object by the values stored under key"""
        self.obj.sort(key=lambda k: k[sort_on_key],reverse=reverse)
        return self.obj

    def keep_if_true(self,function,**kwargs):
        """Each row value is fed into the user supplied function and if the function returns true the row is kept.
        Returns a new JSON_extensions object"""
        rows_kept=[]
        for row in self.obj:
            if function(row,**kwargs)==True:
                rows_kept.append(row)
            else:
                continue
        return JSON_Extensions(rows_kept)

    def assign_header(self,header_list):
        """Takes in a list and assigns this value to self.header"""
        self.header=header_list

    def get_column(self,key):
        """Returns a list of all the values stored under key"""
        column=[]
        for row in self.obj:
            column.append(row[key])
        return column

    def remove_duplicates(self,key):
        """Returns a new JSON_Extension object containing only the rows that first contained the value
        stored under key"""
        found_set=set()
        kept_rows=[]
        for row in self.obj:
            value=row[key]
            if value in found_set:
                continue
            else:
                kept_rows.append(row)
                found_set.add(value)
        return JSON_Extensions(kept_rows)

    def create_histogram(self,key):
        """Returns a dictionary were the values stored under key are the keys and the value is the count of the
        number of times that value appears in the dataset"""

        histogram={}

        for row in self.obj:
            value=row[key]
            if value in histogram:
                histogram[value]+=1
            else:
                histogram[value]=1
        return histogram

    def remove_column_startswith(self,startswith_string):
        """"Removes a column if the header starts with the startswith_string"""


    def add_mult_values_column_from_dic(self,key_col,new_key_name,new_value_dict,delimiter=";"):
        """Takes in the name of the column to be used as a key and a dictionary containing either a tuple,
        list or set and creates a new key name named new_key_name containing the values joined using delimiter"""

        for row in self.obj:
            key=row[key_col]
            if key in new_value_dict:
                row[new_key_name]=delimiter.join(list(new_value_dict[key]))
            else:
                continue

    def in_set_yes_or_no(self,key_col,set,new_column_name="In_Set",yes_value="yes",no_value="no"):
        """Adds a new column containing the value yes_value or no_value depending on if the key_col value is in set"""

        self.header.append(new_column_name)
        for row in self.obj:
            key=row[key_col]
            if key in set:
                row[new_column_name]=yes_value
            else:
                row[new_column_name]=no_value








if __name__=="__main__":
    import unittest
    from csv_to_json import csv_to_json
    from compare_files_tools import get_file_lines

    class Test_JSON_Extensions(unittest.TestCase):

        def setUp(self):
            self.row1={"header1":"Row1.1","header2":"Row1.2","header3":"Row1.3","header4":"Row1.4"}
            self.row2={"header1":"","header2":"Row2.2","header3":"Row2.3","header4":"Row2.4"}
            self.row3={"header1":"Row3.1","header2":"Row3.2","header3":"Row3.3","header4":""}
            self.row4={"header1":"","header2":"Row4.2","header3":"","header4":""}

            self.json_like_object=[self.row1,self.row2,self.row3,self.row4]
            self.json_object2=JSON_Extensions([self.row2])
            self.json_obj=JSON_Extensions(self.json_like_object)

            self.json_like_object2=[{"header1":1,"header2":"a"},{"header1":3,"header2":"c"},{"header1":2,"header2":"b"}]
            self.json_object3=JSON_Extensions(self.json_like_object2)


        def test_object_creation_and_getitem(self):
            self.assertEqual(self.json_obj[0],self.row1)
            self.assertEqual(self.json_obj[1],self.row2)
            self.assertEqual(self.json_obj[2],self.row3)
            self.assertEqual(self.json_obj[3],self.row4)
            self.assertNotEquals(self.json_obj[0],self.row2)

        def test_eq(self):

            correct_answer=self.json_like_object
            self.assertTrue(self.json_obj==correct_answer)

        def test_len(self):
            self.assertEqual(len(self.json_obj),4)

        def test_create_value_set(self):
            correct_answer={"Row1.1","","Row3.1"}
            self.assertEqual(self.json_obj.create_value_set("header1"),correct_answer)

        def test_write_to_json(self):
                outfile="test_data/out_data/sample_out_test_write_to_json"
                self.json_obj.write_to_json(outfile)
                #Open the file as a json object to compare
                f=open(outfile,"r")
                out_obj=json.load(f)
                f.close()
                self.assertEqual(out_obj[0],self.row1)
                self.assertEqual(out_obj[1],self.row2)
                self.assertEqual(out_obj[2],self.row3)
                self.assertEqual(out_obj[3],self.row4)
                self.assertNotEquals(out_obj[0],self.row2)

        def test_write_json_to_csv(self):
            infile="test_data/sample_csv1.csv"
            correct_answer_file="test_data/out_data/test_write_json_to_csv.csv"
            result_file="test_data/out_data/result_write_json_to_csv.csv"
            obj=csv_to_json(infile)
            write_json_to_csv(obj,result_file,obj.header)
            result,reference=get_file_lines(result_file,correct_answer_file)
            self.assertEqual(result,reference)

        def test_filter_by_value_in_other_json(self):
            correct_answer=JSON_Extensions([self.row2,self.row4])
            self.assertEqual(self.json_obj.filter_by_value_in_other_json(self.json_object2,"header1"),correct_answer)

        def test_remove_values_in_set(self):
            removal_set={"Row1.2","Row3.2"}
            key="header2"
            correct_answer=JSON_Extensions([self.row2,self.row4])
            self.assertEqual(self.json_obj.remove_values_in_set(key,removal_set),correct_answer)

        def test_keep_values_in_set(self):
            keep_set={"Row1.1","Row3.1"}
            key="header1"
            correct_answer=JSON_Extensions([self.row1,self.row3])
            self.assertEqual(self.json_obj.keep_values_in_set(key,keep_set),correct_answer)

        def test_keep_n_values_in_set_n2(self):
            keep_set={"Row1.1","Row3.1"}
            key="header1"
            correct_answer=JSON_Extensions([self.row1,self.row3])
            self.assertEqual(self.json_obj.keep_n_values_in_set(key,keep_set,2),correct_answer)

        def test_keep_n_values_in_set_n1(self):
            keep_set={"Row1.1","Row3.1"}
            key="header1"
            correct_answer=JSON_Extensions([self.row1])
            self.assertEqual(self.json_obj.keep_n_values_in_set(key,keep_set,1),correct_answer)


        def test_group_by(self):
            key="header1"
            value="header2"
            correct_answer={"Row1.1":{"Row1.2"},
                            "":{"Row2.2","Row4.2"},
                            "Row3.1":{"Row3.2"}}

            self.assertEqual(self.json_obj.group_by(key,value),correct_answer)

        def test_short_by_key(self):
            correct_answer=[{"header1":1,"header2":"a"},{"header1":2,"header2":"b"},{"header1":3,"header2":"c"}]
            self.assertEqual(self.json_object3.sort_by_key("header2"),correct_answer)

        def test_keep_if_true1(self):
            def header1_blank(row):
                if row["header1"]=="":
                    return True
                else:
                    return False

            correct_answer=[{"header1":"","header2":"Row2.2","header3":"Row2.3","header4":"Row2.4"},
                            {"header1":"","header2":"Row4.2","header3":"","header4":""}]
            self.assertEqual(self.json_obj.keep_if_true(header1_blank),correct_answer)
        def test_keep_if_true2(self):
            def bool_header1_is(row,value):
                if row["header1"]==value:
                    return True
                else:
                    return False
            correct_answer=[{"header1":"","header2":"Row2.2","header3":"Row2.3","header4":"Row2.4"},
                            {"header1":"","header2":"Row4.2","header3":"","header4":""}]

            self.assertEqual(self.json_obj.keep_if_true(bool_header1_is,value=""),correct_answer)


        def test_assign_header(self):
            header=[1,2,3]
            self.json_obj.assign_header(header)
            self.assertEqual(self.json_obj.header,header)

        def test_get_column(self):
            correct_answer=["Row1.2","Row2.2","Row3.2","Row4.2"]
            self.assertEqual(self.json_obj.get_column("header2"),correct_answer)
        def test_remove_duplicates(self):
            correct_answer=[self.row1,self.row2,self.row3]

            self.assertEqual(self.json_obj.remove_duplicates("header1"),correct_answer)

        def test_create_histogram(self):
            correct_answer={"Row1.1":1,"":2,"Row3.1":1}
            self.assertEqual(self.json_obj.create_histogram("header1"),correct_answer)

        def test_add_mult_values_column_from_dic(self):
            mult_values_dic={"Row1.3":["Row5.1","Row5.1.A"],
                             "Row2.3":["Row5.2","Row5.2.B"],
                             "Row3.3": ["Row5.3","Row5.3.C"]}

            correct_answer=JSON_Extensions([{"header1":"Row1.1","header2":"Row1.2","header3":"Row1.3","header4":"Row1.4","header5":"Row5.1;Row5.1.A"},
                            {"header1":"","header2":"Row2.2","header3":"Row2.3","header4":"Row2.4","header5":"Row5.2;Row5.2.B"},
                            {"header1":"Row3.1","header2":"Row3.2","header3":"Row3.3","header4":"","header5":"Row5.3;Row5.3.C"},
                            {"header1":"","header2":"Row4.2","header3":"","header4":""}])
            self.json_obj.add_mult_values_column_from_dic("header3","header5",mult_values_dic)
            self.assertEqual(self.json_obj,correct_answer)

        def test_in_set_yes_or_no(self):
            set={"Row1.1",""}
            #Setting up the correct answer
            row1={"header1":"Row1.1","header2":"Row1.2","header3":"Row1.3","header4":"Row1.4","In_Set":"yes"}
            row2={"header1":"","header2":"Row2.2","header3":"Row2.3","header4":"Row2.4","In_Set":"yes"}
            row3={"header1":"Row3.1","header2":"Row3.2","header3":"Row3.3","header4":"","In_Set":"no"}
            row4={"header1":"","header2":"Row4.2","header3":"","header4":"","In_Set":"yes"}

            obj_answer=[self.row1,self.row2,self.row3,self.row4]
            header_answer=["header1","header2","header3","header4","In_Set"]
            #Create a header so that it can be modified
            self.json_obj.assign_header(["header1","header2","header3","header4"])
            self.json_obj.in_set_yes_or_no("header1",set)
            self.assertEqual(self.json_obj,obj_answer)
            self.assertEqual(self.json_obj.header,header_answer)

    unittest.main()